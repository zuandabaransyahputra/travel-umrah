import express from 'express'
import dotenv from 'dotenv'
import connectDB from './config/db.js'
import morgan from 'morgan'
import { notFound, errorHandler } from './middleware/errorMiddleware.js'
import userRoutes from './routes/userRoutes.js'

//Config env
dotenv.config()

//connect to database
connectDB()

//App
const app = express()
const port = process.env.PORT || 5000

//Morgan
if (process.env.NODE_ENV === 'development') {
    app.use(morgan('dev'))
}

//body parser
app.use(express.json())
app.use(express.urlencoded({ extended: false }))

//Test
app.get('/', (req, res) => {
    res.send("Hello App")
})

//routes
app.use('/api/users', userRoutes)

//error middleware
app.use(notFound)
app.use(errorHandler)

app.listen(port, () => {
    console.log(`Berhasil jalankan server di port: ${port}`)
})