const users = [
    {
        name: 'Ahmad',
        address: 'Banda Aceh',
        description:
            'Travel ini memberikan pelayanan yang sangat baik, sehingga meninggalkan kesan sangat bagus kepada saya, jika kedepan saya mau pergi umrah lagi, saya akan memilih travel ini',
        rating: 5,
        image: '/images/avatar.jpg'
    },
    {
        name: 'Surya',
        address: 'Banda Aceh',
        description:
            'Travel ini memberikan pelayanan yang sangat baik, sehingga meninggalkan kesan sangat bagus kepada saya, jika kedepan saya mau pergi umrah lagi, saya akan memilih travel ini',
        rating: 4,
        image: '/images/avatar.jpg'
    },
    {
        name: 'Aisyah',
        address: 'Banda Aceh',
        description:
            'Travel ini memberikan pelayanan yang sangat baik, sehingga meninggalkan kesan sangat bagus kepada saya, jika kedepan saya mau pergi umrah lagi, saya akan memilih travel ini',
        rating: 5,
        image: '/images/avatar.jpg'
    },
    {
        name: 'Sultan',
        address: 'Banda Aceh',
        description:
            'Travel ini memberikan pelayanan yang sangat baik, sehingga meninggalkan kesan sangat bagus kepada saya, jika kedepan saya mau pergi umrah lagi, saya akan memilih travel ini',
        rating: 3,
        image: '/images/avatar.jpg'
    }
];

export default users;
