import React from 'react';
import Navbar from '../components/Navbar';

const HeroGlobal = () => {
  return (
    <>
      <Navbar />
      <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8 mt-4">
        <img
          src={process.env.PUBLIC_URL + '/images/background.jpg'}
          alt="background"
          className="w-full h-[300px] object-cover object-bottom rounded-lg"
        />
      </div>
    </>
  );
};

export default HeroGlobal;
