const products = [
    {
        id: 1,
        image: '/images/thumbnail.jpg',
        title: 'Umrah Bulan Juni',
        tutor: 'Ustadz Fulan Abu Fulan',
        price: 25000000,
        seat: 30,
        remain: 25,
        rating: 5
    },
    {
        id: 2,
        image: '/images/thumbnail.jpg',
        title: 'Umrah Bulan Juli',
        tutor: 'Ustadz Fulan Abu Fulan',
        price: 23000000,
        seat: 30,
        remain: 10,
        rating: 5
    },
    {
        id: 3,
        image: '/images/thumbnail.jpg',
        title: 'Umrah Bulan Agustus',
        tutor: 'Ustadz Fulan Abu Fulan',
        price: 25000000,
        seat: 30,
        remain: 15,
        rating: 5
    },
    {
        id: 4,
        image: '/images/thumbnail.jpg',
        title: 'Umrah Bulan September',
        tutor: 'Ustadz Fulan Abu Fulan',
        price: 25000000,
        seat: 30,
        remain: 30,
        rating: 5
    }
]

export default products