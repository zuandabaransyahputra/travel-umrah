import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import Footer from './components/Footer';
import HomePage from './Pages/HomePage';
import LoginPage from './Pages/LoginPage';
import RegisterPage from './Pages/RegisterPage';

function App() {
  return (
    <div className='background'>
      <Router>
        <Routes>
          <Route path='/login' element={<LoginPage />} />
          <Route path='/daftar' element={<RegisterPage />} />
          <Route path='/' exact element={<HomePage />} />
        </Routes>
        <Footer />
      </Router>
    </div>
  );
}

export default App;
