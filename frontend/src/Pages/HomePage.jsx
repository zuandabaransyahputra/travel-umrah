import Card from '../components/Card';
import Hero from '../Sections/Hero';
import products from '../products';
import Testimonial from '../components/Testimonial';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import HeroGlobal from '../Sections/HeroGlobal';

export default function HomePage() {
  const userLogin = useSelector(state => state.userLogin);

  const { userInfoTravel } = userLogin;

  return (
    <>
      {userInfoTravel ? (
        <>
          <HeroGlobal />
          <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
            <div className="my-20">
              <p className="text-start text-xl font-semibold text-indigo-600 mb-2">
                Kenapa Kami
              </p>
              <h1 className="text-start text-3xl text-color font-bold mb-8">
                Travel Umrah Berstandar Internasional
              </h1>
              <div className="flex flex-wrap justify-center items-start lg:justify-start lg:flex-nowrap gap-8 lg:gap-4">
                <Card />
              </div>
            </div>
            <div className="my-20">
              <p className="text-start text-xl font-semibold text-indigo-600 mb-2">
                Jadwal
              </p>
              <div className="flex justify-between mb-8">
                <h1 className="text-start text-3xl text-color font-bold">
                  Jadwal Keberangkatan
                </h1>
                <Link
                  to={'/testimonial'}
                  className="hidden md:block rounded-md shadow border border-transparent text-lg lg:text-md font-medium text-indigo-700 bg-indigo-100 hover:bg-indigo-200 py-2 text-md px-4"
                >
                  Cek Tiket
                </Link>
              </div>
              <div className="flex flex-wrap justify-center items-start lg:justify-start lg:flex-nowrap gap-8 lg:gap-4">
                <Card isTicket={true} products={products} />
              </div>
            </div>
            <div className="my-20">
              <p className="text-start text-xl font-semibold text-indigo-600 mb-2">
                Siapa Kami
              </p>
              <h1 className="text-start text-3xl text-color font-bold mb-8">
                Tentang Kami
              </h1>
            </div>
            <div className="my-20">
              <p className="text-start text-xl font-semibold text-indigo-600 mb-2">
                Kata Mereka
              </p>
              <div className="flex justify-between mb-8">
                <h1 className="text-start text-3xl text-color font-bold">
                  Testimonial
                </h1>
                <Link
                  to={'/testimonial'}
                  className="hidden md:block rounded-md shadow border border-transparent text-lg lg:text-md font-medium text-indigo-700 bg-indigo-100 hover:bg-indigo-200 py-2 text-md px-4"
                >
                  Lihat Semua Ulasan
                </Link>
              </div>
              <div className="flex flex-wrap justify-start items-start lg:justify-start lg:flex-nowrap gap-8 lg:gap-4">
                <Testimonial />
              </div>
            </div>
          </div>
        </>
      ) : (
        <>
          <Hero />
          <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
            <div className="my-20">
              <p className="text-start text-xl font-semibold text-indigo-600 mb-2">
                Kenapa Kami
              </p>
              <h1 className="text-start text-3xl text-color font-bold mb-8">
                Travel Umrah Berstandar Internasional
              </h1>
              <div className="flex flex-wrap justify-center items-start lg:justify-start lg:flex-nowrap gap-8 lg:gap-4">
                <Card />
              </div>
            </div>
            <div className="my-20">
              <p className="text-start text-xl font-semibold text-indigo-600 mb-2">
                Jadwal
              </p>
              <div className="flex justify-between mb-8">
                <h1 className="text-start text-3xl text-color font-bold">
                  Jadwal Keberangkatan
                </h1>
                <Link
                  to={'/testimonial'}
                  className="hidden md:block rounded-md shadow border border-transparent text-lg lg:text-md font-medium text-indigo-700 bg-indigo-100 hover:bg-indigo-200 py-2 text-md px-4"
                >
                  Cek Tiket
                </Link>
              </div>
              <div className="flex flex-wrap justify-center items-start lg:justify-start lg:flex-nowrap gap-8 lg:gap-4">
                <Card isTicket={true} products={products} />
              </div>
            </div>
            <div className="my-20">
              <p className="text-start text-xl font-semibold text-indigo-600 mb-2">
                Siapa Kami
              </p>
              <h1 className="text-start text-3xl text-color font-bold mb-8">
                Tentang Kami
              </h1>
            </div>
            <div className="my-20">
              <p className="text-start text-xl font-semibold text-indigo-600 mb-2">
                Kata Mereka
              </p>
              <div className="flex justify-between mb-8">
                <h1 className="text-start text-3xl text-color font-bold">
                  Testimonial
                </h1>
                <Link
                  to={'/testimonial'}
                  className="hidden md:block rounded-md shadow border border-transparent text-lg lg:text-md font-medium text-indigo-700 bg-indigo-100 hover:bg-indigo-200 py-2 text-md px-4"
                >
                  Lihat Semua Ulasan
                </Link>
              </div>
              <div className="flex flex-wrap justify-start items-start lg:justify-start lg:flex-nowrap gap-8 lg:gap-4">
                <Testimonial />
              </div>
            </div>
          </div>
        </>
      )}
    </>
  );
}
