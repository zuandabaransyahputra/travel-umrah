import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import Navbar from '../components/Navbar';
import { useDispatch, useSelector } from 'react-redux';
import Loader from '../components/Loader';
import Message from '../components/Message';
import { register } from '../actions/userActions';

const RegisterPage = () => {
  const [fullName, setFullName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [passwordConfirm, setPasswordConfirm] = useState('');
  const [address, setAddress] = useState('');
  const [message, setMessage] = useState(null);

  const dispatch = useDispatch();
  const navigate = useNavigate();
  const userRegister = useSelector(state => state.userRegister);

  const { loading, error } = userRegister;

  const submitHandler = e => {
    e.preventDefault();
    if (password !== passwordConfirm) {
      setMessage('Password do not match');
    } else {
      dispatch(register(fullName, email, password, address));
      navigate('/login');
    }
  };

  return (
    <>
      <Navbar />
      <div className="min-h-screen flex flex-col">
        <div className="container max-w-md mx-auto flex-1 flex flex-col items-center justify-center px-2">
          {loading && <Loader />}
          {error && <Message message={error} color={'red'} />}
          {message && <Message message={message} color={'red'} />}
          <div className="bg-white px-6 py-8 rounded shadow-md text-black w-full">
            <h1 className="mb-8 text-3xl text-center text-color font-bold">
              Daftar Akun
            </h1>
            <form onSubmit={submitHandler} method="post">
              <input
                type="text"
                className="block border border-grey-light w-full p-3 rounded mb-4"
                name="fullname"
                placeholder="Full Name"
                value={fullName}
                onChange={e => setFullName(e.target.value)}
                required
              />
              <input
                type="email"
                className="block border border-grey-light w-full p-3 rounded mb-4"
                name="email"
                placeholder="Email"
                value={email}
                onChange={e => setEmail(e.target.value)}
                required
              />
              <input
                type="text"
                className="block border border-grey-light w-full p-3 rounded mb-4"
                name="address"
                placeholder="Alamat"
                value={address}
                onChange={e => setAddress(e.target.value)}
                required
              />
              <input
                type="password"
                className="block border border-grey-light w-full p-3 rounded mb-4"
                name="password"
                placeholder="Password"
                value={password}
                onChange={e => setPassword(e.target.value)}
                required
              />
              <input
                type="password"
                className="block border border-grey-light w-full p-3 rounded mb-4"
                name="passwordComfirm"
                placeholder="Confirm Password"
                value={passwordConfirm}
                onChange={e => setPasswordConfirm(e.target.value)}
                required
              />
              <button
                type="submit"
                className="w-full text-center py-3 rounded bg-indigo-600 text-white hover:bg-green-dark focus:outline-none my-1"
              >
                Submit
              </button>
            </form>
          </div>

          <div className="text-color mt-6">
            Sudah memiliki akun?
            <Link className="text-md text-indigo-600 font-bold" to="/login">
              Log in
            </Link>
            .
          </div>
        </div>
      </div>
    </>
  );
};

export default RegisterPage;
