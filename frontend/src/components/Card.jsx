import React from 'react';
import HomeWorkRoundedIcon from '@mui/icons-material/HomeWorkRounded';
import FlightRoundedIcon from '@mui/icons-material/FlightRounded';
import { Link } from 'react-router-dom';
import StarIcon from '@mui/icons-material/Star';

const icons = [
  {
    image: (
      <HomeWorkRoundedIcon
        sx={{ fontSize: 60 }}
        className="bg-indigo-100 rounded-full text-indigo-700 text-center p-2"
      />
    ),
    title: 'Hotel Terbaik',
    description:
      "Kami menyiapkan hotel bintang 5 dan hotel yang dekat dengan Ka'bah",
  },
  {
    image: (
      <FlightRoundedIcon
        sx={{ fontSize: 60 }}
        className="bg-indigo-100 rounded-full text-indigo-700 text-center p-2"
      />
    ),
    title: 'Maskapai Terpercaya',
    description:
      'Kami bekerjasama dengan maskapai terpercaya, agar penerbangan menjadi nyaman',
  },
  {
    image: (
      <FlightRoundedIcon
        sx={{ fontSize: 60 }}
        className="bg-indigo-100 rounded-full text-indigo-700 text-center p-2"
      />
    ),
    title: 'Hotel Terbaik',
    description:
      "Kami menyiapkan hotel bintang 5 dan hotel yang dekat dengan Ka'bah",
  },
  {
    image: (
      <FlightRoundedIcon
        sx={{ fontSize: 60 }}
        className="bg-indigo-100 rounded-full text-indigo-700 text-center p-2"
      />
    ),
    title: 'Hotel Terbaik',
    description:
      "Kami menyiapkan hotel bintang 5 dan hotel yang dekat dengan Ka'bah",
  },
];

const Card = ({ isTicket = false, products }) => {
  return (
    <>
      {isTicket
        ? products.map(product => (
          <div
            key={product.id}
            className="p-5 pb-8 h-full rounded-lg bg-white md:max-w-[45%] lg:w-full lg:max-w-[50%] flex-auto lg:flex-1"
          >
            <img
              src={process.env.PUBLIC_URL + `${product.image}`}
              alt={product.title}
              className="lg:w-[300px] md:w-[400px] h-full rounded-lg"
            />
            <h1 className="text-start text-lg text-color font-bold mt-4">
              {product.title}
            </h1>
            <p className="text-start text-md text-gray-500 font-medium">
              Rp {product.price}
            </p>
            <span>
              {[...Array(product.rating)].map((e, i) => (
                <StarIcon key={i} sx={{ color: 'yellow', fontSize: 18 }} />
              ))}
            </span>
            <h3 className="text-start text-lg md:text-base mt-5 text-color font-bold">
              {product.tutor}
            </h3>
            <div className="flex justify-start items-center gap-5 mb-5">
              <p className="text-gray-500 font-semibold text-md">
                Seat: {product.seat}
              </p>
              <p className="text-gray-500 font-semibold text-md">
                Sisa: {product.remain}
              </p>
            </div>
            <Link
              to={`/tiket/${product.id}`}
              className="rounded-md shadow border border-transparent text-lg lg:text-md font-medium text-white bg-indigo-600 hover:bg-indigo-700 py-2 text-md px-4"
            >
              Details
            </Link>
          </div>
        ))
        : icons.map((icon, index) => (
          <div
            key={index}
            className="p-5 h-full rounded-lg bg-white md:max-w-[45%] lg:w-full lg:max-w-[50%] flex-auto lg:flex-1"
          >
            {icon.image}
            <h1 className="text-start text-xl text-color font-bold mt-4">
              {icon.title}
            </h1>
            <p className="text-start text-lg text-color font-medium mt-4">
              {icon.description}
            </p>
          </div>
        ))}
    </>
  );
};

export default Card;
