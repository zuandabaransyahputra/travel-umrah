import React from 'react';

const Footer = () => {
  return (
    <footer className='my-10'>
      <h1 className="font-medium text-gray-500 hover:text-gray-900 max-w-7xl text-center">
        Copyright &copy; Travel Umrah
      </h1>
    </footer>
  );
};

export default Footer;
