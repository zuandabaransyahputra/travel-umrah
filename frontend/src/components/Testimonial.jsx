import React from 'react';
import users from '../users';
import StarIcon from '@mui/icons-material/Star';

const Testimonial = () => {
  return (
    <>
      {users.map((user, index) => (
        <div
          key={index}
          className="p-5 h-full rounded-lg bg-white md:max-w-[45%] lg:w-full lg:max-w-[50%] flex-auto lg:flex-1"
        >
          <img
            src={process.env.PUBLIC_URL + `${user.image}`}
            alt={user.name}
            className="rounded-full w-[75px] h-[75px] object-cover"
          />
          <h1 className="text-start text-xl text-color font-bold mt-4">
            {user.name}
          </h1>
          <p className="text-start text-md text-gray-500 font-medium">
            {user.address}
          </p>
          <p className="text-start text-sm text-color font-medium mt-4 mb-3">
            {user.description}
          </p>
          <span>
            {[...Array(user.rating)].map((e, i) => (
              <StarIcon key={i} sx={{ color: 'yellow', fontSize: 24 }} />
            ))}
          </span>
        </div>
      ))}
    </>
  );
};

export default Testimonial;
